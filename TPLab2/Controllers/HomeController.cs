﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TPLab2.Database;
using TPLab2.Models;

namespace TPLab2.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly ComputerStoreContext _db;

    public HomeController(
        ILogger<HomeController> logger,
        ComputerStoreContext db)
    {
        _logger = logger;
        _db = db;
    }
    [HttpGet]
    public async Task<IActionResult> Index(string? promocode)
    {
        var withPromocode = _db.Promocodes.SelectMany(p => p.Computers).Select(c => c.Id);
        var computers = await _db.Computers.Where(c => !withPromocode.Contains(c.Id)).ToListAsync();
        if (promocode != null)
        {
            var computersWithPromocode = await _db.Promocodes.Where(p => p.Value == promocode).SelectMany(p => p.Computers).ToListAsync();
            computers.AddRange(computersWithPromocode);
        }
        return View(computers);
    }

    [HttpGet]
    public async Task<IActionResult> Get(Guid id)
    {
        var computer = await _db.Computers.FirstOrDefaultAsync(c => c.Id == id);
        return computer is null ? NotFound() : View(computer);
    }
    [HttpPost]
    public async Task<IActionResult> Delete(Guid id)
    {
        var computer = await _db.Computers.FirstOrDefaultAsync(c => c.Id == id);
        if (computer is null)
            return NotFound();
        _db.Computers.Remove(computer);
        await _db.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    [HttpPost]
    public async Task<IActionResult> Update(Computer computer)
    {
        if (!_db.Computers.Any(c => c.Id == computer.Id))
            return NotFound();
        _db.Computers.Update(computer);
        await _db.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }
     
    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}