﻿using Microsoft.EntityFrameworkCore;
using TPLab2.Models;

namespace TPLab2.Database;

public class ComputerStoreContext : DbContext
{
    public DbSet<Computer> Computers { get; set; }
    public DbSet<Promocode> Promocodes { get; set; }

    public ComputerStoreContext(DbContextOptions<ComputerStoreContext> options, bool useSeed = false) : base(options)
    {
        if (Database.EnsureCreated() && useSeed)
        {
            var computers = SeedComputers();
            var promocodes = SeedPromocode();
            Promocodes.AddRange(promocodes);
            Computers.AddRange(computers);
            SaveChanges();
        }
    }

    private IEnumerable<Computer> SeedComputers()
    {
        var computers = new List<Computer>()
        {
            new()
            {
                Id = Guid.NewGuid(),
                Cpu = "Intel GigaXeon",
                CpuCooler = "Air conditioner",
                GraphicCard = "Gt 1337",
                HardDrive = "SSD Chamchung 512 Mb, 28 Gb/s",
                Monitor = "Philips IPS 320x240px",
                Motherboard = "Petabyte PATAU 17",
                PowerSupplyUnit = "Red horse 512 kWt",
                Ram = "711 Gb quad channel"
            },
            new()
            {
                Id = Guid.NewGuid(),
                Cpu = "Intel GigaAMD",
                CpuCooler = "Window conditioner",
                GraphicCard = "Gt 512 Ultras",
                HardDrive = "SSD ZungZwang 629 Mb, 0.1 Gb/s",
                Monitor = "Philips IPS 5097x240px",
                Motherboard = "Kilobyte Creeper 2007",
                PowerSupplyUnit = "Johnny Walker 40%",
                Ram = "12 MB"
            }
        };
        return computers;
    }
    private IEnumerable<Promocode> SeedPromocode()
    {
        var promocodes = new List<Promocode>()
        {
            new()
            {
                Id = Guid.NewGuid(),
                Value = "PROMOCODE",
                Computers = new List<Computer>()
                {
                    new()
                    {
                        Id = Guid.NewGuid(),
                        Cpu = "Extra1 Intel GigaXeon",
                        CpuCooler = "Extra1 Air conditioner",
                        GraphicCard = "Extra1 Gt 1337",
                        HardDrive = "Extra1 SSD Chamchung 512 Mb, 28 Gb/s",
                        Monitor = "Extra1 Philips IPS 320x240px",
                        Motherboard = "Extra1 Petabyte PATAU 17",
                        PowerSupplyUnit = "Extra1 Red horse 512 kWt",
                        Ram = "Extra1 711 Gb quad channel"
                    },
                    new()
                    {
                        Id = Guid.NewGuid(),
                        Cpu = "Extra2 Intel GigaXeon",
                        CpuCooler = "Extra2 Air conditioner",
                        GraphicCard = "Extra2 Gt 1337",
                        HardDrive = "Extra2 SSD Chamchung 512 Mb, 28 Gb/s",
                        Monitor = "Extra2 Philips IPS 320x240px",
                        Motherboard = "Extra2 Petabyte PATAU 17",
                        PowerSupplyUnit = "Extra2 Red horse 512 kWt",
                        Ram = "Extra2 711 Gb quad channel"
                    }
                }
                
            }
        };
        return promocodes;
    }
}