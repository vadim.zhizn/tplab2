using Microsoft.EntityFrameworkCore;
using TPLab2.Database;
using TPLab2.Models;

var builder = WebApplication.CreateBuilder(args);
var options = new DbContextOptionsBuilder<ComputerStoreContext>().UseInMemoryDatabase("ComputerStore").Options;
builder.Services.AddControllersWithViews();
builder.Services.AddScoped(_ => new ComputerStoreContext(options, true));
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();