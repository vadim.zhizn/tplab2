﻿namespace TPLab2.Models;

public class Promocode
{
    public Guid Id { get; set; }
    public string Value { get; set; }
    public ICollection<Computer>? Computers { get; set; }
}