﻿namespace TPLab2.Models;

public class Computer
{
    public Guid Id { get; set; }
    public string? Cpu { get; set; }
    public string? Ram { get; set; }
    public string? Motherboard { get; set; }
    public string? GraphicCard { get; set; }
    public string? CpuCooler { get; set; }
    public string? HardDrive { get; set; }
    public string? Monitor { get; set; }
    public string? PowerSupplyUnit { get; set; }
}