﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using TPLab2.Controllers;
using TPLab2.Database;

namespace TpLab2.Tests;

public static class DependencyInjection
{
    public static ServiceCollection InitilizeServices()
    {
        var services = new ServiceCollection();
        var options = new DbContextOptionsBuilder<ComputerStoreContext>().UseInMemoryDatabase("ComputerStore").Options;
        services.AddScoped(_ => new ComputerStoreContext(options));
        return services;
    }
}