using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Moq;
using TPLab2.Controllers;
using TPLab2.Database;
using TPLab2.Models;

namespace TpLab2.Tests;

public class HomeControllerTests
{
    private readonly Mock<ILogger<HomeController>> _mock = new();
    private readonly IServiceProvider _serviceProvider;
    public HomeControllerTests()
    {
        _serviceProvider = DependencyInjection.InitilizeServices().BuildServiceProvider();
    }
    [Fact]
    public async Task Get_ReturnsViewResult_WithComputer()
    {
        var db = _serviceProvider.GetRequiredService<ComputerStoreContext>();
        var controller = new HomeController(_mock.Object, db);

        var dbEntry = new Computer()
        {
            Id = Guid.NewGuid()
        };
        db.Computers.Add(dbEntry);
        await db.SaveChangesAsync();
        var result = await controller.Get(dbEntry.Id);

        var viewResult = Assert.IsType<ViewResult>(result);
        var model = Assert.IsAssignableFrom<Computer>(viewResult.ViewData.Model);
        
        Assert.Equal(dbEntry.Id, model.Id);
        
    }
    [Fact]
    public async Task Get_ReturnsNotFound_WhenComputerDoesntExistInDatabase()
    {
        var db = _serviceProvider.GetRequiredService<ComputerStoreContext>();
        var controller = new HomeController(_mock.Object, db);
        
        await db.SaveChangesAsync();
        var result = await controller.Get(Guid.NewGuid());

        Assert.IsType<NotFoundResult>(result);
    }
    [Fact]
    public async Task Delete_ReturnsNotFound_WhenComputerDoesntExistInDatabase()
    {
        var db = _serviceProvider.GetRequiredService<ComputerStoreContext>();
        var controller = new HomeController(_mock.Object, db);
        
        await db.SaveChangesAsync();
        var result = await controller.Delete(Guid.NewGuid());

        Assert.IsType<NotFoundResult>(result);
    }
    
    [Fact]
    public async Task Delete_ReturnsRedirect_WhenComputerRemoved()
    {
        var db = _serviceProvider.GetRequiredService<ComputerStoreContext>();
        var controller = new HomeController(_mock.Object, db);

        var dbEntry = new Computer()
        {
            Id = Guid.NewGuid()
        };
        db.Computers.Add(dbEntry);
        await db.SaveChangesAsync();
        var result = await controller.Delete(dbEntry.Id);

        var viewResult = Assert.IsType<RedirectToActionResult>(result);

        Assert.Null(db.Computers.FirstOrDefault(c => c.Id == dbEntry.Id));
    }
    [Fact]
    public async Task Update_ReturnsNotFound_WhenComputerDoesntExistInDatabase()
    {
        var db = _serviceProvider.GetRequiredService<ComputerStoreContext>();
        var controller = new HomeController(_mock.Object, db);
        
        await db.SaveChangesAsync();
        var result = await controller.Update( new Computer(){ Id = Guid.NewGuid()});

        Assert.IsType<NotFoundResult>(result);
    }
    [Fact]
    public async Task Update_ReturnsRedirect_WhenComputerUpdated()
    {
        var db = _serviceProvider.GetRequiredService<ComputerStoreContext>();
        var controller = new HomeController(_mock.Object, db);

        var dbEntry = new Computer()
        {
            Id = Guid.NewGuid(),
            Cpu = "Val 1"
        };
        var updateEntry = new Computer()
        {
            Id = dbEntry.Id,
            Cpu = "Val 2"
        };
        db.Computers.Add(dbEntry);
        await db.SaveChangesAsync();
        db.ChangeTracker.Clear();
        var result = await controller.Update(updateEntry);

        var viewResult = Assert.IsType<RedirectToActionResult>(result);

        var updatedDbEntry = db.Computers.FirstOrDefault(c => c.Id == dbEntry.Id);
        Assert.Equal(updateEntry.Cpu, updatedDbEntry.Cpu);
    }
    [Fact]
    public async Task Index_ReturnsComputersWithoutPromocode_WhenPromocodeNotEntered()
    {
        var computerWithoutPromocode = new Computer()
        {
            Id = Guid.NewGuid()
        };
        
        var promocode = new Promocode()
        {
            Id = Guid.NewGuid(),
            Value = "PROMO",
            Computers = new List<Computer>()
            {
                new()
                {
                    Id = new Guid(),
                }
            }
        };
        
        var db = _serviceProvider.GetRequiredService<ComputerStoreContext>();
        db.Computers.Add(computerWithoutPromocode);
        db.Promocodes.Add(promocode);
        await db.SaveChangesAsync();
        
        var controller = new HomeController(_mock.Object, db);
        var result = await controller.Index(null);
        var viewResult = Assert.IsType<ViewResult>(result);
        var model = Assert.IsAssignableFrom<IEnumerable<Computer>>(viewResult.ViewData.Model);
        Assert.Single(model);
        Assert.Equal(computerWithoutPromocode.Id, model.First().Id);
        

    }
    [Fact]
    public async Task Index_ReturnsComputersWithPromocode_WhenPromocodeEntered()
    {
        var computerWithoutPromocode = new Computer()
        {
            Id = Guid.NewGuid()
        };
        
        var promocode = new Promocode()
        {
            Id = Guid.NewGuid(),
            Value = "PROMO",
            Computers = new List<Computer>()
            {
                new()
                {
                    Id = new Guid(),
                }
            }
        };
        
        var db = _serviceProvider.GetRequiredService<ComputerStoreContext>();
        db.Computers.Add(computerWithoutPromocode);
        db.Promocodes.Add(promocode);
        await db.SaveChangesAsync();
        
        var controller = new HomeController(_mock.Object, db);
        var result = await controller.Index("PROMO");
        var viewResult = Assert.IsType<ViewResult>(result);
        var model = Assert.IsAssignableFrom<IEnumerable<Computer>>(viewResult.ViewData.Model);
        Assert.Equal(2, model.Count());
        Assert.NotNull(model.FirstOrDefault(m => m.Id == computerWithoutPromocode.Id));
        Assert.NotNull(model.FirstOrDefault(m => m.Id == promocode.Computers.First().Id));


    }
}